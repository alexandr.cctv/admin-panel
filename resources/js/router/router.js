import VueRouter from 'vue-router';
//import Dashboard from '../components/dashboard/Dashboard';
import Table from '../components/table/Table';
import Users from '../components/users/Users';

const routes = [
    // / {path: '/', menu: true, name: 'dashboard', component: Dashboard},
    {
        path: '/panel/settings',
        menu: true,
        name: 'settings',
        icon: 'settings',
        component: Table,
        access: [
            'admin', 'super_admin'
        ]
    },
    {
        path: '/panel/option',
        menu: true,
        parent: 'settings',
        name: 'options',
        icon: 'settings_input_component',
        component: Table,
        access: [
            'admin', 'super_admin'
        ]
    },
    {
        path: '/panel/user',
        menu: true,
        parent: 'settings',
        name: 'users',
        icon: 'account_circle',
        component: Users,
        access: [
            'admin', 'super_admin'
        ]
    },
    {
        path: '/panel/role',
        menu: true,
        parent: 'settings',
        name: 'roles',
        icon: 'device_hub',
        component: Table,
        access: [
            'admin', 'super_admin'
        ]
    },
    {
        path: '/panel/oauth-client',
        menu: true,
        parent: 'settings',
        name: 'oauth clients',
        icon: 'security',
        component: Table,
        access: [
            'admin', 'super_admin'
        ]
    },
    {
        path: '/panel/writer',
        menu: true,
        name: 'writer',
        icon: 'settings',
        component: Table,
        access: [
            'admin', 'super_admin', 'writer'
        ]
    },
    {
        path: '/panel/post',
        menu: true,
        parent: 'writer',
        name: 'posts',
        icon: 'settings',
        component: Table,
        access: [
            'admin', 'super_admin', 'writer'
        ]
    },
];

const router = new VueRouter({
    routes
});

export default router;
