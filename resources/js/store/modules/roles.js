const state = {
    items: [],
}

// getters
const getters = {
    all: (state, getters, rootState) => {
        if (state.items.length <= 0) {
            axios.get('/panel/user-roles')
                .then(response => {
                    state.items = response.data;
                })
                .catch(error => {
                    alert(error);
                    console.log(error);
                });
        }
        return state.items;
    },
    fetchUserRoles: (state, getters, rootState) => {

    }
}

export default {
    namespaced: true,
    state,
    getters,
    /*actions,
    mutations*/
}
