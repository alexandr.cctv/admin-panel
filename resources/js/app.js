require('./bootstrap');

import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import LeftSidebar from './components/sidebar/LeftSidebar'
import TopNav from './components/TopNav'
import router from './router/router';
import store from './store';

Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(VeeValidate);

Vue.component('left-sidebar', LeftSidebar);
Vue.component('top-nav', TopNav);

const app = new Vue({
    router,
    store,
    el: '#app'
});
