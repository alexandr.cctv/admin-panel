<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 18:14
 */

namespace Greenelf\AdminPanel\Traits;

/**
 * Trait ValidationRulesTrait
 *
 * @package App\Services\AdminPanel\Traits
 */
trait ValidationRulesTrait
{
    public function rules():array
    {
        if (!property_exists($this, 'rules')) {
            return [];
        }

        return $this->rules;
    }

    public function getValidationRulesAsStrings(): array
    {
        if (!property_exists($this, 'rules')) {
            return [];
        }

        return $this->rules;
    }
}