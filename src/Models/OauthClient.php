<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/12/18
 * Time: 3:03 PM
 */

namespace Greenelf\AdminPanel\Models;

use Greenelf\AdminPanel\Traits\ValidationRulesTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OauthClient
 * @package Greenelf\AdminPanel\Models
 */
class OauthClient extends Model
{
    use ValidationRulesTrait;

    protected $rules = [
        'user_id' => 'required',
        'name' => 'required|min:5',
        'secret' => 'required|min:6',
    ];

    public $fillable = [
        'user_id',
        'name',
        'secret'
    ];

    public $hidden = [
        'created_at',
        'updated_at',
    ];
}