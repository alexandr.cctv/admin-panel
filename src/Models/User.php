<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 24.06.18
 * Time: 19:21
 */

namespace Greenelf\AdminPanel\Models;

use Greenelf\AdminPanel\Traits\ValidationRulesTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends \App\User
{
    use ValidationRulesTrait, SoftDeletes, HasApiTokens, HasRoles;
    protected $guard_name = 'web';
    protected $rules = [
	    'name' => 'required',// varchar(255) not null,
	    'email' => 'required|email',// varchar(255) not null,
	    'password' => 'required|min:6',// varchar(255) not null,
    ];

    protected $dates = ['deleted_at'];
}
