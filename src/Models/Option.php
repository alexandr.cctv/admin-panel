<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/12/18
 * Time: 4:29 PM
 */
namespace Greenelf\AdminPanel\Models;

use Greenelf\AdminPanel\Traits\ValidationRulesTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Option
 * @package Greenelf\AdminPanel\Models
 */
class Option extends Model
{
    use ValidationRulesTrait;

    public $table = 'options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value'
    ];

    protected $rules = [
        'key' => 'required',
        'value' => 'required'
    ];
}
