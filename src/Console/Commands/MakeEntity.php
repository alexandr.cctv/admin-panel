<?php
/**
 * User: Alex Chesnui
 * Date: 12.06.18
 * Time: 14:32
 */
namespace Greenelf\AdminPanel\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

/**
 * Class MakeEntity
 *
 * @package Greenelf\AdminPanel\Console\Commands
 */
class MakeEntity extends Command
{
    protected $files;

    protected $signature = 'make:entity {model_name}';

    protected $description = 'Create a new controller and resources classes for admin panel';

    /**
     * MakeEntity constructor.
     *
     * @param \Illuminate\Filesystem\Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Handle command
     *
     * @return bool
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $modelName = $this->prepareClassName();

        if (!$this->files->exists(base_path().'/app/Http/Resources')) {
            $this->files->makeDirectory(base_path().'/app/Http/Resources');
        }
        if (!$this->files->exists(base_path().'/app/Http/Resources/AdminPanel')) {
            $this->files->makeDirectory(base_path().'/app/Http/Resources/AdminPanel');
        }
        if (!$this->files->exists(base_path().'/app/Http/Controllers/AdminPanel')) {
            $this->files->makeDirectory(base_path().'/app/Http/Controllers/AdminPanel');
        }

        if (!$this->alreadyExists($this->getPath($modelName, 'Models'))) {
            $this->error($modelName.' model not found!');

            return false;
        }

        if ($this->alreadyExists($this->getPath($modelName.'Controller', 'Http/Controllers/AdminPanel'))) {
            $this->error($modelName.'Controller already exists!');

            return false;
        }

        if ($this->alreadyExists($this->getPath($modelName, 'Http/Resources/AdminPanel'))) {
            $this->error($modelName.' resource already exists!');

            return false;
        }

        if ($this->alreadyExists($this->getPath($modelName.'Collection', 'Http/Resources/AdminPanel'))) {
            $this->error($modelName.'Collection already exists!');

            return false;
        }

        $this->files->put($this->getPath($modelName, 'Http/Resources/AdminPanel'), $this->buildResource($modelName));
        $this->info($modelName.' resource created successfully.');
        $this->files->put($this->getPath($modelName.'Collection', 'Http/Resources/AdminPanel'), $this->buildCollection($modelName));
        $this->info($modelName.'Collection created successfully.');
        $this->files->put($this->getPath($modelName.'Controller', 'Http/Controllers/AdminPanel'), $this->buildController($modelName));
        $this->info($modelName.'Controller created successfully.');
    }

    /**
     * Build controller
     *
     * @param string $modelName
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildController(string $modelName):string
    {
        $stub = $this->files->get($this->getStub('Controller'));

        return $this->replaceClass($stub, $modelName.'Controller');
    }

    /**
     * Build resource collection
     *
     * @param string $modelName
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildCollection(string $modelName): string
    {
        $stub = $this->files->get($this->getStub('ResourceCollection'));

        return $this->replaceClass($stub, $modelName.'Collection');
    }

    /**
     * Build resource
     *
     * @param string $modelName
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildResource(string $modelName): string
    {
        $stub = $this->files->get($this->getStub('Resource'));

        return $this->replaceClass($stub, $modelName);
    }

    /**
     * Replace class name
     *
     * @param string $stub
     * @param string $name
     *
     * @return string
     */
    protected function replaceClass(string $stub, string $name): string
    {
        return str_replace('DummyClass', $name, $stub);
    }

    /**
     * Check file exists
     *
     * @param string $rawName
     *
     * @return bool
     */
    protected function alreadyExists(string $rawName):bool
    {
        return $this->files->exists($rawName);
    }


    /**
     * Prepare Class name
     *
     * @return string
     */
    protected function prepareClassName(): string
    {
        $className = $this->argument('model_name');

        return strtoupper(substr($className, 0, 1)).substr($className, 1);
    }


    /**
     * Get the stub file for the generator.
     *
     * @param string $name
     *
     * @return string
     */
    protected function getStub(string $name): string
    {
        return __DIR__.'/Stubs/'.$name.'.stub';
    }

    /**
     * @param $name
     * @return string
     */
    protected function getPath(string $name, string $projectFolder)
    {
        return base_path().'/app/'.$projectFolder.'/'.str_replace('\\', '/', $name).'.php';
    }
}