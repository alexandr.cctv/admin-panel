<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 24.06.18
 * Time: 18:52
 */

namespace Greenelf\AdminPanel\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

/**
 * Class Install
 * @package Greenelf\AdminPanel\Console\Commands
 */
class Install extends Command
{
    protected $signature = 'panel:install';

    protected $description = 'Install admin panel';

    /**
     * Handle command
     */
    public function handle()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $this->seedRoles();
        $this->seedUser();
        Artisan::call('passport:install');
        $this->info('Install sucsessful');
        $this->info('Email: admin@dashboard.io');
        $this->info('Password: admin (change IT!!!)');
    }

    /**
     * Seed default user
     */
    private function seedUser()
    {
        $user = \App\User::create([
            'email' => 'admin@dashboard.io',
            'name' => 'admin',
            'password' => Hash::make('admin')
        ]);

       $user->assignRole('super_admin');
    }

    /**
     * Seed two default roles
     */
    private function seedRoles()
    {
        Role::create(['name' => 'super_admin']);
        Role::create(['name' => 'admin']);
    }
}
