<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.06.18
 * Time: 20:40
 */
namespace Greenelf\AdminPanel\Utils\Database;

use Greenelf\AdminPanel\Traits\ValidationRulesTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DatabaseInfo
{
    /**
     * Return table structure with rules and types
     * @param Model $entity
     * @return array
     */
    public function getTableStructure(Model $entity): array
    {
        $columns = $this->getTableColumns($entity);
        $modelSchema = [];
        $modelFillableFields = $entity->getFillable();
        $validationRules = (in_array(
            ValidationRulesTrait::class,
            class_uses($entity)
        )) ? $entity->getValidationRulesAsStrings() : [];

        foreach ($columns as $column) {
            $colName = $column->getName();
            if (!in_array($colName, $modelFillableFields)) {
                continue;
            }
            $rules = '';

            if (array_key_exists($colName, $validationRules)) {
                $rules = $validationRules[$colName];
            }
            $modelSchema[$colName] = [
                'name' => $colName,
                'type' => (string)$column->getType(),
                'rules' => $rules,
            ];
        }

        return $modelSchema;
    }

    /**
     * Return table columns info
     * @param Model $model
     * @return array
     */
    public function getTableColumns(Model $model):array
    {
        $schema = DB::connection()->getDoctrineSchemaManager();
        $columns = $schema->listTableColumns($model->getTable());
        return $columns;
    }
}