<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.06.18
 * Time: 20:36
 */
namespace Greenelf\AdminPanel\Utils\Excel\Exceptions;

/**
 * Class ExcelImportException
 * @package Greenelf\AdminPanel\Utils\Excel\Exceptions
 */
class ExcelImportException extends \Exception
{

}