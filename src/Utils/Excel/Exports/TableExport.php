<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.06.18
 * Time: 16:51
 */
namespace Greenelf\AdminPanel\Utils\Excel\Exports;

use Illuminate\Database\Eloquent\Model;
use Greenelf\AdminPanel\Utils\Database\DatabaseInfo;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

/**
 * Class TableExport
 * @package Greenelf\AdminPanel\Utils\Excel\Exports
 */
class TableExport implements FromCollection, WithHeadings
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * TableExport constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Return array for create headers in excel
     * @return array
     */
    public function headings(): array
    {
        $db = new DatabaseInfo();

        return array_keys($db->getTableColumns($this->model));
    }

    /**
     * Return model collection for export
     * @return Collection
     */
    public function collection():Collection
    {
        return collect(DB::table($this->model->getTable())->get())->map(function ($item) {
            return (array)$item;
        });
    }
}