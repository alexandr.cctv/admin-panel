<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.06.18
 * Time: 18:00
 */
namespace Greenelf\AdminPanel\Utils\Excel;

use Greenelf\AdminPanel\Utils\Database\DatabaseInfo;
use Greenelf\AdminPanel\Utils\Excel\Exceptions\ExcelImportException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Class Import
 * @package Greenelf\AdminPanel\Utils\Excel
 */
class Import
{
    protected $model;

    /**
     * Import constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Import data from excel file
     * @param string $filePath
     * @throws ExcelImportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function import(string $filePath)
    {
        $spreadsheet = IOFactory::load($filePath);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $this->clearNullFields($sheetData);
        $this->truncateTable();
        $this->fillTable($sheetData);
    }

    /**
     * Clear trash data
     * @param array $data
     * @throws ExcelImportException
     */
    private function clearNullFields(array &$data)
    {
        $fields = $this->getColumnsNames(array_shift($data));
        
        $this->validteFields($fields);
        
        $data = array_map(function($item) use ($fields){
            $temp = [];
            foreach ($fields as $key => $value) {
                $temp[$value] = $item[$key];
            }
            return $temp;
        }, $data);
    }

    /**
     * @param array $data
     * @return array
     */
    private function getColumnsNames(array $data): array
    {
        return array_filter($data, function($item){
            return !is_null($item);
        });
    }

    /**
     * @param array $fields
     * @throws ExcelImportException
     */
    private function validteFields(array $fields)
    {
        $dbInfo = new DatabaseInfo();
        $tableField = array_keys($dbInfo->getTableColumns($this->model));

        if (count(array_diff($fields, $tableField)) !== 0) {
            throw new ExcelImportException('Incorrect dataset got['.json_encode($fields).'] need ['.json_encode($tableField).']');
        }
    }

    /**
     * truncate table data
     */
    private function truncateTable()
    {
        $table = $this->model->getTable();
        DB::transaction(function () use ($table){
            DB::table($table)->delete();
        });
    }

    /**
     * Fill table
     * @param array $data
     */
    private function fillTable(array $data)
    {
        $table = $this->model->getTable();
        DB::transaction(function() use ($table, $data){
            DB::table($table)->insert($data);
        });
    }
}