<?php

namespace Greenelf\AdminPanel\Policies;

use App\User;
use Greenelf\AdminPanel\Models\Option;
use Illuminate\Auth\Access\HandlesAuthorization;

class OptionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the option.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasRole('super_admin');
    }

    /**
     * Determine whether the user can view the option.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function view(User $user, Option $option)
    {
        //
    }

    /**
     * Determine whether the user can create options.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the option.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function update(User $user, Option $option)
    {
        //
    }

    /**
     * Determine whether the user can delete the option.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function delete(User $user, Option $option)
    {
        //
    }

    /**
     * Determine whether the user can restore the option.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function restore(User $user, Option $option)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the option.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function forceDelete(User $user, Option $option)
    {
        //
    }
}
