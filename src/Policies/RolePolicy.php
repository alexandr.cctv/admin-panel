<?php

namespace Greenelf\AdminPanel\Policies;

use App\User;
use Greenelf\AdminPanel\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the option.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function view(User $user, Role $role)
    {
        //
    }

    /**
     * Determine whether the user can create options.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function update(User $user, Role $role)
    {
        //
    }

    /**
     * Determine whether the user can delete the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function destroy(User $user, Role $role)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can restore the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function restore(User $user, Role $role)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function forceDelete(User $user, Role $role)
    {
        //
    }
}
