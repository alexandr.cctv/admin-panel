<?php

namespace Greenelf\AdminPanel\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the option.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasRole('super_admin');
    }

    /**
     * Determine whether the user can view the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function view(User $user, User $managedUser)
    {
        //
    }

    /**
     * Determine whether the user can create options.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyRole([
            'super_admin'
        ]);
    }

    public function store(User $user)
    {
        return $user->hasAnyRole([
            'super_admin'
        ]);
    }

    /**
     * Determine whether the user can update the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function update(User $user, User $managedUser)
    {
        return $user->hasAnyRole([
            'super_admin'
        ]);
    }

    /**
     * Determine whether the user can delete the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function destroy(User $user, User $managedUser)
    {
        return $user->hasRole('super_admin');
    }

    /**
     * Determine whether the user can restore the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function restore(User $user, User $managedUser)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the option.
     *
     * @param  \App\User  $user
     * @param  \App\User  $managedUser
     * @return mixed
     */
    public function forceDelete(User $user, User $managedUser)
    {
        //
    }
}
