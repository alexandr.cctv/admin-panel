@extends('AdminPanel::admin')
@section('content')
    <section class="section">
        <div class="container has-text-centered">
            <h1 class="title">
                Login
            </h1>
            <form method="post" action="{{route('panel.register')}}">
                {{ csrf_field() }}
                <div class="field">
                    <label class="label">Name</label>
                    <div class="control has-icons-left has-icons-right">
                        <input name="name" class="input" type="text" placeholder="Name" value="{{old('name')}}">
                        <span class="icon is-small is-left">
      <i class="fas fa-user"></i>
    </span>
                        @if($errors->has('name'))
                            <span class="icon is-small is-right">
      <i class="fas fa-exclamation-triangle"></i>
    </span>
                        @endif
                    </div>
                    @if($errors->has('name'))
                        @foreach($errors->get('name') as $error)
                            <p class="help is-danger">{{$error}}</p>
                        @endforeach
                    @endif
                </div>
                <div class="field">
                    <label class="label">Email</label>
                    <div class="control has-icons-left has-icons-right">
                        <input name="email" class="input" type="email" placeholder="Email" value="{{old('email')}}">
                        <span class="icon is-small is-left">
      <i class="fas fa-envelope"></i>
    </span>
                        @if($errors->has('email'))
                            <span class="icon is-small is-right">
      <i class="fas fa-exclamation-triangle"></i>
    </span>
                        @endif
                    </div>
                    @if($errors->has('email'))
                        @foreach($errors->get('email') as $error)
                            <p class="help is-danger">{{$error}}</p>
                        @endforeach
                    @endif
                </div>
                <div class="field">
                    <label class="label">Password</label>
                    <div class="control has-icons-right">
                        <input name="password" class="input" type="password" placeholder="Password">
                        @if($errors->has('password'))
                            <span class="icon is-small is-right">
      <i class="fas fa-exclamation-triangle"></i>
    </span>
                        @endif
                    </div>
                    @if($errors->has('password'))
                        @foreach($errors->get('password') as $error)
                            <p class="help is-danger">{{$error}}</p>
                        @endforeach
                    @endif
                </div>
                <div class="field">
                    <label class="label">Confirm password</label>
                    <div class="control has-icons-right">
                        <input name="password_confirmation" class="input" type="password" placeholder="Confirm password">
                        @if($errors->has('password_confirmation'))
                            <span class="icon is-small is-right">
      <i class="fas fa-exclamation-triangle"></i>
    </span>
                        @endif
                    </div>
                    @if($errors->has('password_confirmation'))
                        @foreach($errors->get('password_confirmation') as $error)
                            <p class="help is-danger">{{$error}}</p>
                        @endforeach
                    @endif
                </div>
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-link">Register</button>
                    </div>
                    <div class="control">
                        <a class="button is-text" href="{{url('/')}}">Back to main page</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
