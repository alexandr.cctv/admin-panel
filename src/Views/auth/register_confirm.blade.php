@extends('AdminPanel::admin')
@section('content')
    <section class="section">
        <div class="container has-text-centered">
            <h1 class="title">
                You are registered but our administrator have to verification you
            </h1>
            <div class="columns">
                <div class="column is-half is-offset-one-quarter">
                    Wait register result. We will send verification result to your email: {{$user->email}}
                    <p>
                        <a class="button is-text" href="{{url('/')}}">Back to main page</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
