@extends('AdminPanel::admin')
@section('content')
    <section class="section">
        <div class="container has-text-centered">
            <h1 class="title">
                Login
            </h1>
            <form method="post" action="{{route('panel.login')}}">
                {{ csrf_field() }}
                <div class="field">
                    <label class="label">Email</label>
                    <div class="control has-icons-left has-icons-right">
                        <input name="email" class="input" type="email" placeholder="Email" value="{{old('email')}}">
                        @if($errors->has('email'))
                        @endif
                    </div>
                    @if($errors->has('email'))
                        @foreach($errors->get('email') as $error)
                            <p class="help is-danger">{{$error}}</p>
                        @endforeach
                    @endif
                </div>
                <div class="field">
                    <label class="label">Password</label>
                    <div class="control has-icons-right">
                        <input name="password" class="input" type="password" placeholder="Password">
                        @if($errors->has('password'))
                            <span class="icon is-small is-right">
      <i class="fas fa-exclamation-triangle"></i>
    </span>
                        @endif
                    </div>
                    @if($errors->has('password'))
                        @foreach($errors->get('password') as $error)
                            <p class="help is-danger">{{$error}}</p>
                        @endforeach
                    @endif
                </div>
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-link">Login</button>
                    </div>
                    <div class="control">
                        <a class="button is-text" href="{{url('/')}}">Back to main page</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
