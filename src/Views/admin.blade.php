<!DOCTYPE html>
<html lang="en" class="gr__getbootstrap_com">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin panel</title>
    <link rel="stylesheet" href="{{ url('css/panel.css') }}">
<body data-gr-c-s-loaded="true">
@yield('content')
<div id="app">
    <v-app>
        <v-navigation-drawer app>
            <left-sidebar></left-sidebar>
        </v-navigation-drawer>
        <top-nav></top-nav>
        <v-content>
            <v-container fluid>
                <router-view></router-view>
            </v-container>
        </v-content>
        <v-footer app></v-footer>
    </v-app>
</div>
<script src="js/panel.js"></script>

</body>
</html>
