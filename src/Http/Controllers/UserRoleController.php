<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 21:23
 */
namespace Greenelf\AdminPanel\Http\Controllers;

use App\User;
use Greenelf\AdminPanel\Http\Requests\UpdateUserRolesRequest;
use Illuminate\Routing\Controller;

/**
 * Class UserRoleController
 * @package Greenelf\AdminPanel\Http\Controllers
 */
class UserRoleController extends Controller
{
    public function index()
    {
        return response()->json(\Auth::user()->getRoleNames());
    }
    
    public function update(UpdateUserRolesRequest $request)
    {
        $user = User::find($request->route('user_role'));
        $user->syncRoles($request->roles);
        return response()->json($request->roles);
    }
}
