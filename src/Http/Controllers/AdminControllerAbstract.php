<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 18:09
 */

namespace Greenelf\AdminPanel\Http\Controllers;

use Greenelf\AdminPanel\Events\ImportEvent;
use Greenelf\AdminPanel\Utils\Database\DatabaseInfo;
use Greenelf\AdminPanel\Utils\Excel\Exports\TableExport;
use Greenelf\AdminPanel\Utils\Excel\Import;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class AdminControllerAbstract
 *
 * @package App\Services\AdminPanel\Http\Controllers
 */
abstract class AdminControllerAbstract extends Controller
{
    use AuthorizesRequests;

    protected $modelClass;
    protected $model;
    protected $request;
    protected $filter;

    /**
     * AdminControllerAbstract constructor.
     * AdminControllerAbstract constructor.
     * @param Request $request
     * @param string $model
     * @throws ValidationException
     */
    public function __construct(Request $request, string $model = '')
    {
        if (app()->runningInConsole()) {
            return;
        }
        $this->model = $model;
        $this->request = $request;

        if (!$this->model) {
            $this->model = $this->getModelFromUrl($request);
        }

        $this->checkData([
            'model' => $this->model
        ], [
            'model' => 'required'
        ]);

        $this->modelClass = $this->findModel($this->model);

        $this->checkData([
            'model' => $this->modelClass
        ], [
            'model' => 'required'
        ], [
            'required' => 'Model not found!!!'
        ]);

        $this->setFilter($request);//todo
    }

    private function setFilter(Request $request)
    {
        if ($this->filter !== null) {
            return;
        }

        if ($request->parameter1 != 'filter' || !$request->parameter2 || !$request->parameter3) {
            return;
        }

        $this->modelClass = $this->modelClass::where(strtolower($request->parameter2), 'like', '%'.$request->parameter3.'%');
    }

    /**
     * Return all entities
     * @return BinaryFileResponse
     * @throws ValidationException
     */
    public function index()
    {
        $this->authorize(__FUNCTION__, substr($this->modelClass, 1));

        if ($this->request->has('export')) {
            return $this->exportToExcel();
        }

        $resourceClass = $this->findResource($this->model);

        $this->checkData([
            'resource' => $resourceClass
        ], [
            'resource' => 'required'
        ], [
            'required' => 'Resource not found!!!'
        ]);

        if (!$this->modelClass instanceof Builder) {
            $this->modelClass = $this->modelClass::whereNotNull('id');
        }

        if ($this->request->has('sort_by')) {
            if ((bool)$this->request->has('descending')) {
                $this->modelClass->orderBy($this->request->get('sort_by'));
            }else {
                $this->modelClass->orderByDesc($this->request->get('sort_by'));
            }
        }

        return new $resourceClass($this->modelClass->paginate());
    }

    /**
     * create new entity
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function create()
    {
        $this->authorize(__FUNCTION__, substr($this->modelClass, 1));
        $entity = new $this->modelClass();

        $this->checkData([
            'model' => $entity
        ], [
            'model' => 'required'
        ], [
            'required' => 'Model not found!!!'
        ]);


        $modelSchema = $this->getTableStructure($entity);

        return response()->json(
            [
                'data' => $entity,
                'schema' => $modelSchema,
            ]
        );
    }

    /**
     * Edit entity
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function edit(int $id)
    {
        $this->authorize(__FUNCTION__, substr($this->modelClass, 1));
        $entity = $this->modelClass::find($id);

        $this->checkData([
            'model' => $entity
        ], [
            'model' => 'required'
        ], [
            'required' => 'Item not found!!! ID=' . $id
        ]);

        $modelSchema = $this->getTableStructure($entity);

        return response()->json(
            [
                'data' => $entity,
                'schema' => $modelSchema,
            ]
        );
    }

    /**
     * Delete entity
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function destroy(int $id)
    {
        $entity = $this->modelClass::find($id);
        $this->authorize(__FUNCTION__, $entity);
        $this->checkData([
            'model' => $entity,
            'delete_status' => $entity->delete()
        ], [
            'model' => 'required',
            'delete_status' => 'accepted'
        ], [
            'required' => 'Model not found!!! ID=' . $id,
            'delete_status' => 'Delete fail!!!' . $id
        ]);

        return $this->successJsonResponse($id);
    }

    /**
     * Update entity
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function update(int $id, Request $request)
    {
        $this->authorize(__FUNCTION__, substr($this->modelClass, 1));
        $entity = $this->modelClass::find($id);

        $this->checkData([
            'model' => $entity,
        ], [
            'model' => 'required'
        ], [
            'required' => 'Model item not found!!! ID=' . $id
        ]);

        $this->checkData($request->all(), $entity->rules());

        foreach ($request->all() as $columnName => $value) {
            $entity->$columnName = $value;
        }

        $this->checkData([
            'update_status' => $entity->save()
        ], [
            'update_status' => 'accepted'
        ], [
            'update_status' => 'Update fail!!!' . $id
        ]);

        return $this->successJsonResponse($id);
    }

    /**
     * Create new instance or import table
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws ValidationException
     * @throws \Greenelf\AdminPanel\Utils\Excel\Exceptions\ExcelImportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function store(Request $request)
    {
        $this->authorize(__FUNCTION__, substr($this->modelClass, 1));
        if ($request->has('file')) {
            return $this->importFromExcel($request->file('file'));
        }

        $entity = new $this->modelClass();

        $this->checkData([
            'model' => $entity,
        ], [
            'model' => 'required'
        ], [
            'required' => 'Model not found!!!'
        ]);

        $this->checkData($request->all(), $entity->rules());

        foreach ($request->all() as $columnName => $value) {
            $entity->$columnName = $value;
        }

        $this->checkData([
            'create_status' => $entity->save()
        ], [
            'create_status' => 'accepted'
        ], [
            'create_status' => 'Create fail!!!'
        ]);

        return $this->successJsonResponse($entity->id);
    }

    /**
     * Return response with relations
     *
     * @param JsonResponse $response
     * @param array $relations ['value'=>value, 'label' => value]
     * @return JsonResponse
     * @throws \Exception
     */
    protected function withRelations(JsonResponse $response, array $relations = [])
    {
        $this->checkRelationsForResponse($relations);
        $data = $response->getData();
        $data->relations = $relations;
        return $response->setData($data);
    }

    /**
     * Check model relations for response
     * @param array $relations
     * @throws \Exception
     */
    private function checkRelationsForResponse(array &$relations)
    {
        foreach ($relations as $relation => $data) {
            $data->map(function($item) use ($relation){
                if (!$item->value === null || !$item->label === null) {
                    throw new \Exception('Model require correct ['.$relation.']relations structure');
                }
            });
        }
    }


    /**
     * Find resource
     *
     * @param string $modelName
     *
     * @return null|string
     */
    protected function findResource(string $modelName)
    {
        $resorceName = $modelName . 'Collection';
        $this->findFiles($finder, $resorceName, [
            app()->basePath() . "/app/Http/Resources/AdminPanel",
            __DIR__ . "/../Resources"
        ]);

        foreach ($finder as $file) {
            $namespace = $this->getNamespaceFromContent($file->getContents());
            if ($namespace) {
                return $namespace . $resorceName;
            }
        }

        return null;
    }

    /**
     * Find model class
     *
     * @param string $modelName
     *
     * @return null|string
     */
    protected function findModel(string $modelName): ?string
    {
        $this->findFiles($finder, $modelName, [
            __DIR__ . "/../../Models",
            app()->basePath() . "/app/Models",
        ]);

        foreach ($finder as $file) {
            $namespace = $this->getNamespaceFromContent($file->getContents());
            if ($namespace) {
                return $namespace . $modelName;
            }
        }

        return null;
    }

    /**
     * Find files
     *
     * @param \Symfony\Component\Finder\Finder $finder
     * @param string $modelName
     */
    private function findFiles(&$finder, string $modelName, array $directories)
    {
        $directories = array_filter($directories, function ($dir) {
            return file_exists($dir);
        });

        $finder = new Finder();
        $finder->files()->in($directories)->name(
            $modelName . ".php"
        );
    }

    /**
     * Return namespace from file content
     *
     * @param string $content
     *
     * @return null|string
     */
    private function getNamespaceFromContent(string $content): ?string
    {
        preg_match_all(
            '/namespace (.*)\;/m',
            $content,
            $matches,
            PREG_SET_ORDER,
            0
        );

        if (isset($matches[0]) && isset($matches[0][1])) {
            return "\\" . $matches[0][1] . "\\";
        }

        return null;
    }

    /**
     * Return model from url
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return null|string
     */
    protected function getModelFromUrl(Request $request): ?string
    {
        $segments = $request->segments();

        if (count($segments) < 2) {
            return null;
        }
        $result = explode('-', ucfirst($segments[1]));

        if (count($result) == 1) {
            return $result[0];
        }

        foreach ($result as $key => $value) {
            $result[$key] = ucfirst($value);
        }

        return implode('', $result);

    }

    /**
     * Return entity table structure
     *
     * @param \Illuminate\Database\Eloquent\Model $entity
     *
     * @return array
     */
    protected function getTableStructure(Model $entity): array
    {
        $dbInfo = new DatabaseInfo();
        return $dbInfo->getTableStructure($entity);
    }

    /**
     * Check data from request
     * @param array $data
     * @param array $ruls
     * @param array $messages
     * @throws ValidationException
     */
    protected function checkData(array $data, array $ruls, array $messages = [])
    {
        $validator = Validator::make($data, $ruls, $messages);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    /**
     * Destroy success json response
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function successJsonResponse(int $id)
    {
        return response()->json(
            [
                'id' => $id,
            ]
        );
    }

    /**
     * Export model to excel
     * @return BinaryFileResponse
     */
    protected function exportToExcel(): BinaryFileResponse
    {
        return Excel::download(new TableExport(new $this->modelClass()), $this->model . '.xlsx');
    }

    /**
     * Import data from excel
     * @param UploadedFile $file
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Greenelf\AdminPanel\Utils\Excel\Exceptions\ExcelImportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    protected function importFromExcel(UploadedFile $file)
    {
        $filePath = $file->getRealPath();

        $importer = new Import(new $this->modelClass);

        $importer->import($filePath);

        event(new ImportEvent(new $this->modelClass));

        return response('Table ' . $this->model . ' import finished', 200);
    }
}
