<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 20:56
 */
namespace Greenelf\AdminPanel\Http\Controllers;

use Illuminate\Routing\Controller;

/**
 * Class DashboardController
 *
 * @package Greenelf\AdminPanel\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Return main view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('AdminPanel::admin');
    }
}