<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 21:23
 */
namespace Greenelf\AdminPanel\Http\Controllers\Entities;

use Greenelf\AdminPanel\Models\User;
use Greenelf\AdminPanel\Http\Controllers\AdminControllerAbstract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserController
 *
 * @package Greenelf\AdminPanel\Http\Controllers\Entities
 */
class UserController extends AdminControllerAbstract
{
    public function userRoles(Request $request)
    {
        $user = \App\User::findOrFail($request->route('user'));
        return response()->json($user->getRoleNames());
    }

    public function store(Request $request)
    {
        $this->authorize(__FUNCTION__, \App\User::class);
        $newUser = new User();
        $this->checkData($request->all(), $newUser->rules());
        $newUser->fill($request->all());
        $newUser->password = Hash::make($request->password);
        $newUser->save();
        return response()->json($newUser);
    }
    public function update(int $id, Request $request)
    {
        $this->authorize(__FUNCTION__,\App\User::class);
        $entity = \App\User::find($id);

        $this->checkData([
            'model' => $entity,
        ], [
            'model' => 'required'
        ], [
            'required' => 'Model item not found!!! ID=' . $id
        ]);
        $rules = $entity->rules();
        unset($rules['passord']);
        $this->checkData($request->all(), $rules);
        $entity->fill($request->all());
        $this->checkData([
            'update_status' => $entity->save()
        ], [
            'update_status' => 'accepted'
        ], [
            'update_status' => 'Update fail!!!' . $id
        ]);
        return $this->successJsonResponse($id);
    }
}
