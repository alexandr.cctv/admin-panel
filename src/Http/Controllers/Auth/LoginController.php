<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 20:07
 */
namespace Greenelf\AdminPanel\Http\Controllers\Auth;

use Greenelf\AdminPanel\Http\Requests\Auth\LoginRequest;
use Greenelf\AdminPanel\Http\Requests\Auth\RegisterRequest;
use Greenelf\AdminPanel\Models\User;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;

/**
 * Class LoginController
 *
 * @package Greenelf\AdminPanel\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/panel';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show login form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('AdminPanel::auth.login');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegisterForm()
    {
        return view('AdminPanel::auth.register');
    }

    /**
     * Register new user
     * @param RegisterRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function register(RegisterRequest $request)
    {
        $fields = $request->all();
        $fields['password'] = Hash::make($fields['password']);
        $newUser = new User();
        $newUser->fill($fields);
        $newUser->save();
        $newUser->delete();

        return view('AdminPanel::auth.register_confirm', ['user' => $newUser]);
    }

    /**
     * Login user
     *
     * @param \Greenelf\AdminPanel\Http\Requests\Auth\LoginRequest $request
     *
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(LoginRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}