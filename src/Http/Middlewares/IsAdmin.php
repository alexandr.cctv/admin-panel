<?php
/**
 * User: Alex Chesnui
 * Date: 12.06.18
 * Time: 11:50
 */
namespace Greenelf\AdminPanel\Http\Middlewares;

use Closure;
use Greenelf\AdminPanel\Models\UserRole;

/**
 * Class IsAdmin
 *
 * @package Greenelf\AdminPanel\Http\Middlewares
 */
class IsAdmin
{
    /**
     * Check admin permissions
     *
     * @param          $request
     * @param \Closure $next
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && UserRole::isAdmin($request->user()->id)) {
            return $next($request);
        }

        return redirect(route('loginForm'));
    }
}