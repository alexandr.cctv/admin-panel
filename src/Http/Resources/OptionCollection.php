<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/12/18
 * Time: 4:31 PM
 */
namespace Greenelf\AdminPanel\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class OptionCollection
 * @package Greenelf\AdminPanel\Http\Resources
 */
class OptionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }
}
