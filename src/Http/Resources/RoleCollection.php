<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 21:37
 */
namespace Greenelf\AdminPanel\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class RoleCollection
 * @package Greenelf\AdminPanel\Http\Resources
 */
class RoleCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }
}
