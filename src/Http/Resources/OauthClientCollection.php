<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/12/18
 * Time: 2:30 PM
 */
namespace Greenelf\AdminPanel\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class OauthClientCollection
 * @package Greenelf\AdminPanel\Http\Resources
 */
class OauthClientCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }
}
