<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/12/18
 * Time: 4:31 PM
 */
namespace Greenelf\AdminPanel\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Option
 * @package Greenelf\AdminPanel\Http\Resources
 */
class Option extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
