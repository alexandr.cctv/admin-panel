<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 21:37
 */

namespace Greenelf\AdminPanel\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class User
 *
 * @package Greenelf\AdminPanel\Http\Resources
 */
class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
