<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 18:02
 */
Route::group(
    [
        'prefix' => '/panel',
        'middleware' => ['web'],
    ],
    function () {
        Route::get(
            '/login', [
                'as' => 'loginForm',
                'uses' => 'Auth\LoginController@showLoginForm'
        ]);
        Route::post(
            '/login', [
            'as' => 'panel.login',
            'uses' => 'Auth\LoginController@login'
        ]);
        Route::get(
            '/register',
            [
                'as' => 'registerForm',
                'uses' => 'Auth\LoginController@showRegisterForm'
            ]
        );
        Route::post(
            'register',
            [
                'as' => 'panel.register',
                'uses' => 'Auth\LoginController@register'
            ]
        );
    }
);

Route::group(
    [
        'prefix' => '/panel',
        'middleware' => ['web', 'role:admin|super_admin|writer'],
    ],
    function () {
        Route::get('logout', [
            'as' => 'logout',
            'uses' => 'Auth\LoginController@logout'
        ]);
        Route::get(
            '/', [
            'as' => 'panel',
            'uses' => 'DashboardController@index'
        ]);

        Route::resources(
            [
                '/user' => 'Entities\UserController',
                '/user-roles' => 'UserRoleController',
                '/role' => 'Entities\RoleController',
                '/oauth-client' => 'Entities\OauthClientController',
                '/option' => 'Entities\OptionController',
            ]
        );
        //custom routes
        Route::get('/user/roles/{user}', 'Entities\UserController@userRoles');
    }
);
