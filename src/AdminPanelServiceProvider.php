<?php
/**
 * User: Alex Chesnui
 * Date: 11.06.18
 * Time: 17:57
 */
namespace Greenelf\AdminPanel;

use App\User;
use Greenelf\AdminPanel\Models\User as PanelUser;
use Greenelf\AdminPanel\Console\Commands\MakeEntity;
use Greenelf\AdminPanel\Console\Commands\Install;
use Greenelf\AdminPanel\Http\Middlewares\IsAdmin;
use Greenelf\AdminPanel\Models\Option;
use Greenelf\AdminPanel\Models\Role;
use Greenelf\AdminPanel\Policies\OptionPolicy;
use Greenelf\AdminPanel\Policies\RolePolicy;
use Greenelf\AdminPanel\Policies\UserPolicy;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\PassportServiceProvider;
use Maatwebsite\Excel\ExcelServiceProvider;
use Maatwebsite\Excel\Facades\Excel;
use Laravel\Passport\Passport;
use Spatie\Permission\PermissionServiceProvider;
use Illuminate\Support\Facades\Gate;

/**
 * Class AdminPanelServiceProvider
 *
 * @package Greenelf\AdminPanel
 */
class AdminPanelServiceProvider extends ServiceProvider
{
    protected $policies = [
        Option::class => OptionPolicy::class,
        User::class => UserPolicy::class,
        PanelUser::class => UserPolicy::class,
        Role::class => RolePolicy::class,
    ];
    
    protected $providers = [
        ExcelServiceProvider::class,
        PassportServiceProvider::class,
        PermissionServiceProvider::class,
    ];

    protected $facades = [
        'Excel' => Excel::class
    ];

    protected $middlewares = [
       // 'isAdmin' => IsAdmin::class,
        'client' => \Laravel\Passport\Http\Middleware\CheckClientCredentials::class,
        'scopes' => \Laravel\Passport\Http\Middleware\CheckScopes::class,
        'scope' => \Laravel\Passport\Http\Middleware\CheckForAnyScope::class,
        'role' => \Spatie\Permission\Middlewares\RoleMiddleware::class,
        'permission' => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
        'role_or_permission' => \Spatie\Permission\Middlewares\RoleOrPermissionMiddleware::class,
    ];

    /**
     * Run when app is booting
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerRoutes();

        $this->loadViews();

        $this->loadMigrations();

        $this->loadMiddlewares();

        $this->addPublishes();

        $this->loadCommands();

        $this->loadServiceProviders();


        Gate::before(function ($user, $ability) {
            return $user->hasRole('super_admin') ? true : null;
        });
    }

    /**
     * Actions when service is calling
     */
    public function register()
    {

    }

    private function registerPolicies()
    {
        foreach ($this->policies as $key => $value) {
            Gate::policy($key, $value);
        }
    }

    /**
     * Load package mmigrations
     */
    private function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }

    /**
     * Load views
     */
    private function loadViews()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'AdminPanel');
    }

    /**
     * Register routes and controllers namespace
     */
    private function registerRoutes()
    {
        $this->app->router->group([
            'namespace' => 'Greenelf\AdminPanel\Http\Controllers'
        ], function (){
            require __DIR__.'/routes/routes.php';
        });

        Passport::routes();
    }

    /**
     * Load middlewares
     */
    private function loadMiddlewares()
    {
        foreach ($this->middlewares as $middleware => $middlewareClass) {
            $this->app->router->aliasMiddleware($middleware, $middlewareClass);
        }
    }

    /**
     * Add publishes
     */
    private function addPublishes()
    {
        $this->publishes([
            __DIR__.'/../public/css' => public_path('css'),
        ], 'public');
        $this->publishes([
            __DIR__.'/../public/js' => public_path('js'),
        ], 'public');

        $this->publishes([
            __DIR__.'/../resources' => base_path('resources/panel'),
        ], 'resources');
    }

    /**
     * Load package commands
     */
    private function loadCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                MakeEntity::class,
                Install::class
            ]);
        }
    }

    /**
     * Register service providers
     */
    private function loadServiceProviders()
    {
        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        foreach ($this->facades as $key => $facade) {
            $loader->alias($key, $facade);
        }
    }
}
