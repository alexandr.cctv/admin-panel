<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 01.07.18
 * Time: 14:26
 */
namespace Greenelf\AdminPanel\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;

/**
 * Class ImportEvent
 * @package Greenelf\AdminPanel\Events
 */
class ImportEvent
{
    use SerializesModels;

    public $model;

    /**
     * ImportEvent constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}